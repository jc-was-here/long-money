require 'test_helper'

class MyitemsControllerTest < ActionController::TestCase
  setup do
    @myitem = myitems(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:myitems)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create myitem" do
    assert_difference('Myitem.count') do
      post :create, myitem: { quantity: @myitem.quantity, sku: @myitem.sku }
    end

    assert_redirected_to myitem_path(assigns(:myitem))
  end

  test "should show myitem" do
    get :show, id: @myitem
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @myitem
    assert_response :success
  end

  test "should update myitem" do
    patch :update, id: @myitem, myitem: { quantity: @myitem.quantity, sku: @myitem.sku }
    assert_redirected_to myitem_path(assigns(:myitem))
  end

  test "should destroy myitem" do
    assert_difference('Myitem.count', -1) do
      delete :destroy, id: @myitem
    end

    assert_redirected_to myitems_path
  end
end

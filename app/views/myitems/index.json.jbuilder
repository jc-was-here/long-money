json.array!(@myitems) do |myitem|
  json.extract! myitem, :id, :sku, :quantity
  json.url myitem_url(myitem, format: :json)
end

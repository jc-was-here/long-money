class Myitem < ActiveRecord::Base
  validates :item_name, presence: true
  
  validates :sku, presence: true
  
  validates :quantity, presence: true
  
  validates :item_description, presence: true
  
  before_validation :uppercase_sku
  
  def uppercase_sku
    self.sku.upcase!
  end
  
end

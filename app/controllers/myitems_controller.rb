class MyitemsController < ApplicationController
  before_action :set_myitem, only: [:show, :edit, :update, :destroy]

  # GET /myitems
  # GET /myitems.json
  def index
    @myitems = Myitem.all
  end

  # GET /myitems/1
  # GET /myitems/1.json
  def show
  end

  # GET /myitems/new
  def new
    @myitem = Myitem.new
  end

  # GET /myitems/1/edit
  def edit
  end

  # POST /myitems
  # POST /myitems.json
  def create
    @myitem = Myitem.new(myitem_params)

    respond_to do |format|
      if @myitem.save
        format.html { redirect_to @myitem, notice: 'Myitem was successfully created.' }
        format.json { render action: 'show', status: :created, location: @myitem }
      else
        format.html { render action: 'new' }
        format.json { render json: @myitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /myitems/1
  # PATCH/PUT /myitems/1.json
  def update
    respond_to do |format|
      if @myitem.update(myitem_params)
        format.html { redirect_to @myitem, notice: 'Myitem was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @myitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /myitems/1
  # DELETE /myitems/1.json
  def destroy
    @myitem.destroy
    respond_to do |format|
      format.html { redirect_to myitems_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_myitem
      @myitem = Myitem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def myitem_params
      params.require(:myitem).permit(:item_name, :sku, :quantity, :item_description)
    end
end

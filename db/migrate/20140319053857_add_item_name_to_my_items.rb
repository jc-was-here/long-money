class AddItemNameToMyItems < ActiveRecord::Migration
  def change
    add_column :myitems, :item_name, :string
    add_column :myitems, :item_description, :text
  end
end

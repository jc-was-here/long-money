class CreateMyitems < ActiveRecord::Migration
  def change
    if connection.tables.include?('myitems')
      drop_table :myitems
    end
    
    create_table :myitems do |t|
      t.string :sku
      t.integer :quantity

      t.timestamps
    end
  end
end
